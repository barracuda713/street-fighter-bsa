# Street Fighter
The ES6 task in BSA-2022.

Init commit is a clone of [stage-2-es6-for-everyone](https://github.com/binary-studio-academy/stage-2-es6-for-everyone).

## Demo
[Street Fighter](https://street-fighter-bsa.vercel.app/)


## Instalation

`npm install`

`npm run build`

`npm run start`

open http://localhost:9000/