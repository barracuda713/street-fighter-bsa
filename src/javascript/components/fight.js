import { controls } from '../../constants/controls';
import { randomChance } from '../helpers/mathHelper';
import { Fighter } from '../helpers/fighterHelper';

export async function fight(fighters) {
  const firstFighter = new Fighter(fighters[0], "PlayerOne", "left-fighter-indicator");
  const secondFighter = new Fighter(fighters[1], "PlayerTwo", "right-fighter-indicator");

  return new Promise((resolve) => {
    let keysPressed = new Set();

    const hit = (attacker, defender) => {
      if (keysPressed.has(controls[`${attacker.nik}Block`])) return;
      
      if (!keysPressed.has(controls[`${defender.nik}Block`])) {
        const damage = getDamage(attacker, defender);
        if (attacker.isHitAnotherFighterAndWin(defender, damage)) winFight(attacker, defender);
      } else {
        defender.updateStats({damBlocked: getHitPower(attacker)});
        attacker.updateStats({hitBlockedNumber: 1});
      }
    };
    const superHit = (attacker, defender) => {
      if (controls[`${attacker.nik}CriticalHitCombination`].some((k) => !keysPressed.has(k))) return;
      
      const damage = getCriticalHit(attacker);
      if (attacker.isSuperHitAnotherFighterAndWin(defender, damage)) winFight(attacker, defender);
    };

    const winFight = (winner, loser) => {
      removeListeners();
      resolve({winner, loser});
    };

    const keyDownListener = (e) => {
      keysPressed.add(e.code);

      switch (e.code) {
        case controls.PlayerOneAttack:
          hit(firstFighter, secondFighter);
          break;
        case controls.PlayerTwoAttack:
          hit(secondFighter, firstFighter);
          break;
        default:
          break;
      }

      if (!firstFighter.cooldown) {
        superHit(firstFighter, secondFighter);
      }
      if (!secondFighter.cooldown) {
        superHit(secondFighter, firstFighter);
      }
    };
    const keyUpListener = (e) => {
      keysPressed.delete(e.code);
    };

    document.body.addEventListener('keydown', keyDownListener);
    document.body.addEventListener('keyup', keyUpListener);

    const removeListeners = () => {
      document.body.removeEventListener('keydown', keyDownListener);
      document.body.removeEventListener('keyup', keyUpListener);
    };
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return (damage <= 0) ? 0 : damage;
}

export function getHitPower(fighter) {
  const criticalHitChance = randomChance();
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = randomChance();
  return fighter.defense * dodgeChance;
}

export function getCriticalHit(fighter) {
  return fighter.attack * 2;
}