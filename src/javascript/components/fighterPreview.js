import { createElement, createTextDataElement } from '../helpers/domHelper';
import { controls } from '../../constants/controls';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  if (fighter) {
    const { name } = fighter;

    const fighterNameElement = createElement({
      tagName: 'div',
      className: 'fighter-preview__name'
    });
    fighterNameElement.innerHTML = name;

    const fighterDataElement = createElement({
      tagName: 'div',
      className: 'fighter-preview__info'
    });
    fighterDataElement.innerHTML = '';

    const fighterInfoArray = ['health', 'attack', 'defense'];
    const fighterInfoCollection = fighterInfoArray.map((e) => {
      const info = createElement({
        tagName: 'div',
        className: `fighter-preview__info-${e}`
      });
      const img = createElement({
        tagName: 'div',
        className: 'fighter-preview__info-img'
      });
      const data = createElement({
        tagName: 'span',
        className: ''
      });
      data.innerHTML = fighter[e];

      info.append(img, data);

      return info;
    })
    fighterDataElement.append(...fighterInfoCollection);

    const fighterHintsInfo = createElement({
      tagName: 'div',
      className: 'fighter-preview__hints'
    });
    fighterHintsInfo.append(
      createTextDataElement('div', controls[`${fighter.imgId}Attack`].replace(/key/gi, '')),
      createTextDataElement('div', controls[`${fighter.imgId}Block`].replace(/key/gi, '')),
      createTextDataElement('div', controls[`${fighter.imgId}CriticalHitCombination`].join(' + ').replace(/key/gi, ''))
    )
    
    fighterElement.append(createFighterImage(fighter), fighterNameElement, fighterDataElement, fighterHintsInfo);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    id: fighter.imgId,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
