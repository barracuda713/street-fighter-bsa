import { showModal } from './modal';
import { createElement, createTextDataElement } from '../../helpers/domHelper';
import App from '../../app';

export function showWinnerModal(winner, loser = {}) {  
  const modalBody = createElement({
    tagName: 'div',
    className: 'modal-body'
  });

  const tryBtn = createElement({
    tagName: 'button',
    className: 'tryAgainBtn'
  });
  const onClick = () => {
    const root = document.getElementById('root');
    root.innerHTML = '';
    App.startApp();
  };
  tryBtn.addEventListener('click', onClick, false);
  tryBtn.innerText = 'Revenge?';

  const stats = createElement({
    tagName: 'div',
    className: 'fightersStats'
  });
  const table = createElement({
    tagName: 'table',
    className: 'fightersStats-table'
  });
  let row = createElement({tagName: 'tr'});
  let cellArr = [
    createTextDataElement('th', winner.name),
    createTextDataElement('th'),
    createTextDataElement('th', loser.name),
  ];
  row.append(...cellArr);
  table.append(row);
  const statsData = ['Dealt', 'Received', 'Blocked'];
  for (let i = 0; i < statsData.length; i++) {
    row = createElement({tagName: 'tr'});
    cellArr = [
      createTextDataElement('td', Math.round(winner.stats[`dam${statsData[i]}`])),
      createTextDataElement('td', statsData[i]),
      createTextDataElement('td', Math.round(loser.stats[`dam${statsData[i]}`])),
    ];
    row.append(...cellArr);
    table.append(row);
  }
  row = createElement({tagName: 'tr'});
  cellArr = [
    createTextDataElement('td', `${winner.hitAccuracy}%`),
    createTextDataElement('td', 'Hit accuracy'),
    createTextDataElement('td', `${loser.hitAccuracy}%`),
  ];
  row.append(...cellArr);
  table.append(row);
  stats.append(table);

  const dropWrapper = createElement({
    tagName: 'div',
    className: 'dropWrapper'
  });
  const drop = createElement({
    tagName: 'div',
    className: 'drop'
  });
  dropWrapper.append(drop, drop.cloneNode(true), drop.cloneNode(true));

  modalBody.append(stats, tryBtn, dropWrapper);

  showModal({
    title: `${winner.name} wins`, 
    bodyElement: modalBody
  });
}
