import { CriticalHitCombinationCooldown as cooldown } from '../../constants';
import { CriticalHitCombinationAnimationCooldown as animationCooldown } from '../../constants';

export class Fighter {
  constructor(fighter, nik, indicatorId) {
    for (const prop in fighter) {
      this[prop] = fighter[prop];
    }
    this.maxHealth = fighter.health;
    this.cooldown = false;
    this.nik = nik;
    this.indicator = document.getElementById(indicatorId);
    this.img = document.getElementById(nik);
    this.stats = {
      damReceived: 0,
      damDealt: 0,
      damBlocked: 0,
      hitBlockedNumber: 0,
      hitNumber: 0,
      criticalHitNumber: 0
    };
  }

  get healthPercentage() {
    return this.#calcHealthPercentage();
  }

  #calcHealthPercentage() {
    return Math.ceil(this.health / this.maxHealth * 100);
  }

  get hitAccuracy() {
    return this.#calcHitAccuracy();
  }

  #calcHitAccuracy() {
    let hits = (this.stats.hitBlockedNumber + this.stats.hitNumber);
    return hits ? Math.round(this.stats.hitNumber * 100 / hits) : 0;
  }

  #updateIndicator(width) {
    if (this.indicator.style.width !== `${width}%`) {
      this.indicator.classList.add("blink");
      setTimeout(() => this.indicator.classList.remove("blink"), 500);
      this.indicator.style.width = `${width}%`;
    }
  }

  getDamage(damage) {
    this.health -= damage;
    this.updateStats({damReceived: damage});
  }

  checkHealth() {
    let indicatorWidth = (this.health > 0) ? this.healthPercentage : 0;
    this.#updateIndicator(indicatorWidth);
    return indicatorWidth;
  }

  updateStats(stats) {
    for (const p in this.stats) {
      this.stats[p] += stats[p] || 0;
    }
  }

  isHitAnotherFighterAndWin(anotherFighter, damage) {
    anotherFighter.getDamage(damage);
    this.updateStats({
      damDealt: damage,
      hitNumber: 1
    });
    return !anotherFighter.checkHealth();
  }

  isSuperHitAnotherFighterAndWin(anotherFighter, superDamage) {
    this.#changeFightersImgSuperHit();
    this.cooldown = true;
    setTimeout(() => this.cooldown = false, cooldown);
    this.updateStats({criticalHitNumber: 1});
    return this.isHitAnotherFighterAndWin(anotherFighter, superDamage);
  }
  
  #changeFightersImgSuperHit() {
    this.img.src = this.source_critical || this.source;
    setTimeout(() => this.img.src = this.source, animationCooldown);
  }
};