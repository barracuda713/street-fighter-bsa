export const fighters = [
  {
    _id: '1',
    name: 'Ryu',
    source: 'https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif',
  },
  {
    _id: '2',
    name: 'Dhalsim',
    source: 'https://i.pinimg.com/originals/c0/53/f2/c053f2bce4d2375fee8741acfb35d44d.gif',
  },
  {
    _id: '3',
    name: 'Guile',
    source: 'https://66.media.tumblr.com/tumblr_lq8g3548bC1qd0wh3o1_400.gif',
  },
  {
    _id: '4',
    name: 'Zangief',
    source: 'https://media1.giphy.com/media/nlbIvY9K0jfAA/source.gif',
  },
  {
    _id: '5',
    name: 'Ken',
    source: 'https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif',
  },
  {
    _id: '6',
    name: 'Bison',
    source: 'https://www.fightersgeneration.com/np5/char/ssf2hd/bison-hdstance.gif',
  },
];

export const fightersDetails = [
  {
    _id: '1',
    name: 'Ryu',
    health: 45,
    attack: 4,
    defense: 3,
    source: 'https://media.giphy.com/media/kdHa4JvihB2gM/giphy.gif',
    source_win: 'https://www.fightersgeneration.com/np7/char/gifs/ryu/ryu-cfe-tie.gif',
    source_lose: 'https://www.fightersgeneration.com/characters3/ryu-timeout.gif',
    source_critical: 'https://www.fightersgeneration.com/characters3/ryu-cvs-hadoken-a.gif'
  },
  {
    _id: '2',
    name: 'Dhalsim',
    health: 60,
    attack: 3,
    defense: 1,
    source: 'https://i.pinimg.com/originals/c0/53/f2/c053f2bce4d2375fee8741acfb35d44d.gif',
    source_win: 'https://www.fightersgeneration.com/characters/dhalsim-alphawin.gif',
    source_lose: 'https://www.fightersgeneration.com/characters/dhalsim-diz.gif',
    source_critical: 'https://www.fightersgeneration.com/characters/dhalsim-yogafire1a.gif'
  },
  {
    _id: '3',
    name: 'Guile',
    health: 45,
    attack: 4,
    defense: 3,
    source: 'https://66.media.tumblr.com/tumblr_lq8g3548bC1qd0wh3o1_400.gif',
    source_win: 'https://www.fightersgeneration.com/characters/Guile_win_flex.gif',
    source_lose: 'https://www.fightersgeneration.com/characters/sf-guile3.gif',
    source_critical: 'https://www.fightersgeneration.com/characters/sf-guile.gif'
  },
  {
    _id: '4',
    name: 'Zangief',
    health: 60,
    attack: 4,
    defense: 1,
    source: 'https://media1.giphy.com/media/nlbIvY9K0jfAA/source.gif',
    source_win: 'https://www.fightersgeneration.com/characters/zangief-dahh.gif',
    source_lose: 'https://www.fightersgeneration.com/characters/zangief-diz.gif',
    source_critical: 'https://www.fightersgeneration.com/characters/zangief-fireballstop.gif'
  },
  {
    _id: '5',
    name: 'Ken',
    health: 45,
    attack: 3,
    defense: 4,
    source: 'https://i.pinimg.com/originals/46/4b/36/464b36a7aecd988e3c51e56a823dbedc.gif',
    source_win: 'https://www.fightersgeneration.com/characters2/ken-intro2.gif',
    source_lose: 'https://www.fightersgeneration.com/characters2/ken-cvs-time.gif',
    source_critical: 'https://www.fightersgeneration.com/characters2/sf-ken.gif'
  },
  {
    _id: '6',
    name: 'Bison',
    health: 45,
    attack: 5,
    defense: 4,
    source: 'http://www.fightersgeneration.com/np5/char/ssf2hd/bison-hdstance.gif',
    source_win: 'https://www.fightersgeneration.com/characters/bison-sc-win2.gif',
    source_lose: 'https://www.fightersgeneration.com/characters/bison-p-taunt.gif',
    source_critical: 'https://www.fightersgeneration.com/characters/bison-sc-psycho.gif'
  },
];
